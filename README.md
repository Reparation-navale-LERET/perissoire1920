# périssoire1920


## Histoire

https://fr.wikipedia.org/wiki/P%C3%A9rissoire

La périssoire est un type de canot long d'environ 3,5 mètres à 4 mètres pour la périssoire monoplace destinée à la balade, à près de 8 mètres pour la « périssoire de course »1.

Sa construction à bouchain vif est le plus souvent très simple, parfois deux planches pour les côtés et une pour le fond du bateau suffisent. C'est en général un bateau étroit et instable donc périlleux, d’où son nom.
Historique

L'origine de la périssoire est très ancienne, elle est issue de l'évolution de la pirogue monoxyle (creusée dans un seul tronc d'arbre). On la trouve dans le sud des États-Unis sous l'appellation Cajun pirogue, la « pirogue cadienne », en référence à son utilisation par les Français établis en Louisiane2.

Elle fut très en vogue comme petit canot de plaisance aux XIXe et XXe siècles en Europe3,4 avant d'être détrônée par les canoës importés du Canada durant la première moitié du XXe siècle. Elle se manœuvre à la pagaie double, ce qui l'apparente à l'embarcation cousine le kayak (d'origine inuite). Elle s'en différencie par sa construction en planches de bois, et non pas en ossature bois ou os recouverte de peau ou de toile, et ses sections souvent quasi rectangulaires.

La périssoire a constitué un sujet de tableaux pour Gustave Caillebotte, très adepte de nautisme, et pour Maurice de Vlaminck5. Alfred Jarry en posséda une à partir de 1896, appelée L'As.

Comme l'étymologie périr l'y incite, on utilise ce mot péjorativement pour désigner une embarcation facilement chavirable.

Il est possible de retrouver des plans et un manuel de construction qui ont été décrites au travers de l'ouvrage de F. Sergent Construction de canoës et kayaks paru en 19456. 

***

## Name
Périssoire est le nom de ce type de bateau, j'ai donc choisi de le conserver.

## Description
Relever effectuer à partir d'une périssoire qu'un collectionneur m’a donné pour la retaper (voir photos). Les plans ont été édités sous Rhino 3 d. Il s'agit d'une périssoire des années 1920 en contreplaqué sur latte de résineux, assemblé par clouage et collage.

## Contributing
les contributions sont les bienvenus sous respect de la licence ci-dessous.

## Authors and acknowledgment
Léret Camille et jean-Claude Schlossmacher
Pour l'entreprise Réparation Navale Léret

## License
CC-BY-SA
